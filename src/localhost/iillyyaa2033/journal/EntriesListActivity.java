package localhost.iillyyaa2033.journal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ListView;
import localhost.iillyyaa2033.journal.model.Database;
import localhost.iillyyaa2033.journal.model.JournalEntry;

public class EntriesListActivity extends Activity {

	EntriesListAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);

		if (metrics.widthPixels < 500 || metrics.heightPixels < 500) {
			setTheme(R.style.AppTheme);
		}

		setContentView(R.layout.dialog_entrieslist);
		
		ListView ll = (ListView) findViewById(R.id.dialog_entrieslist_list);
		adapter = new EntriesListAdapter(this);
		ll.setAdapter(adapter);
		
		adapter.setListener(new EntriesListAdapter.OnButtonClick(){

				@Override
				public void edit(JournalEntry entry) {
					Intent i = new Intent(EntriesListActivity.this,EntryEditionActivity.class);
					i.putExtra("journalentry_id", entry.id);
					startActivityForResult(i,55);
				}

				@Override
				public void remove(final JournalEntry entry) {
					new AlertDialog.Builder(EntriesListActivity.this)
						.setMessage(String.format(getString(R.string.remove_warn, entry.getFancyHeader())))
						.setNegativeButton(android.R.string.no,null)
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener(){

							@Override
							public void onClick(DialogInterface p1, int p2) {
								new Database(EntriesListActivity.this).removeEntry(entry.id).close();
								adapter.reloadList();
								adapter.notifyDataSetChanged();
							}
						})
						.show();
				}
			});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		adapter.reloadList();
		adapter.notifyDataSetChanged();
	}
}
