package localhost.iillyyaa2033.journal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupMenu;
import android.widget.TextView;
import java.util.ArrayList;
import localhost.iillyyaa2033.journal.bookmark.BookmarksAlphabeticalFragment;
import localhost.iillyyaa2033.journal.bookmark.BookmarksChainFragment;
import localhost.iillyyaa2033.journal.model.BookmarksListener;
import localhost.iillyyaa2033.journal.model.Database;
import localhost.iillyyaa2033.journal.model.JournalEntry;
import localhost.iillyyaa2033.journal.model.JournalModel;
import localhost.iillyyaa2033.journal.model.TesDateFormatter;
import localhost.iillyyaa2033.sync.api.ScheduledSyncAPI;
import org.json.JSONObject;
import android.widget.ImageButton;
import android.view.Window;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String textClose, textJournal;

	private TextView page1, page1num, page2, page2num;
	private Button prev, next, options, close, menu, addBtn;

	private View bookmarksRoot;

	private static Database db = null;
	private JournalModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
		setContentView(R.layout.main);

		// Getting localized string resources
		textClose = getString(R.string.close);
		textJournal = getString(R.string.journal);
		TesDateFormatter.months = getResources().getStringArray(R.array.tes_months);

		// Getting references to UI elements
		page1 = (TextView) findViewById(R.id.main_page1);
		page2 = (TextView) findViewById(R.id.main_page2);
		page1num = (TextView) findViewById(R.id.main_page1_num);
		page2num = (TextView) findViewById(R.id.main_page2_num);

		page1.setLinksClickable(true);
		page1.setMovementMethod(LinkMovementMethod.getInstance());
		if (page2 != null) {
			page2.setLinksClickable(true);
			page2.setMovementMethod(LinkMovementMethod.getInstance());
		}

		setLayoutListner(page1);

		prev = (Button) findViewById(R.id.main_btn_prev);
		next = (Button) findViewById(R.id.main_btn_next);
		options = (Button) findViewById(R.id.main_btn_options);
		close = (Button) findViewById(R.id.main_btn_close);

		prev.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					model.prev();
					updateNavBar();
				}
			});

		next.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					model.next();
					updateNavBar();
				}
			});

		options.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					showBookmarkAnimated();
				}
			});

		close.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					if (textClose.equalsIgnoreCase(close.getText().toString())) {
						ExitActivity.exit(MainActivity.this);
					} else {
						loadMainList();
					}
				}
			});

		addBtn = (Button) findViewById(R.id.main_btn_add);
		if (addBtn != null) {
			addBtn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View p1) {
						Intent i = new Intent(MainActivity.this, EntryEditionActivity.class);
						startActivity(i);
					}
				});
		}

		bookmarksRoot = findViewById(R.id.main_bookmarks_root);

		getFragmentManager()
			.beginTransaction()
			.add(R.id.bookmarks_overlay_frame, new Fragment())
			.commit();
		loadBookmarkAlphabetical();

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
		int w = metrics.widthPixels < 300 ? LayoutParams.MATCH_PARENT : 300;
		int h = metrics.heightPixels < 450 ? LayoutParams.MATCH_PARENT : 450 ;
		// TODO: set 'w' and 'h' as width and height of bookmarksRoot's view

		Button bCancel = (Button) findViewById(R.id.bookmarks_overlay_btn_cancel);
		bCancel.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					hideBookmarkAnimated();
				}
			});

		Button bmksThemes = (Button) findViewById(R.id.bookmarks_overlay_themes);
		bmksThemes.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					loadBookmarkAlphabetical();
				}
			});
		bmksThemes.setOnLongClickListener(new OnLongClickListener(){

				@Override
				public boolean onLongClick(View p1) {
					hideBookmarkAnimated();
					Intent i = new Intent(MainActivity.this, EntryEditionActivity.class);
					startActivity(i);
					return true;
				}
			});

		Button bmksQuests = (Button) findViewById(R.id.bookmarks_overlay_quests);
		bmksQuests.setOnLongClickListener(new OnLongClickListener(){

				@Override
				public boolean onLongClick(View p1) {
					openSettings();
					return true;
				}
			});
		bmksQuests.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					loadBookmarkQuests();
				}
			});

		menu = (Button) findViewById(R.id.bookmarks_overlay_menu);
		menu.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					PopupMenu pm = new PopupMenu(MainActivity.this, p1);
					Menu mn = pm.getMenu();
					mn.add(0, 0, 0, R.string.new_record);
					mn.add(1, 1, 1, R.string.preferences);
					mn.add(2, 2, 2, R.string.records_list);
					pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){

							@Override
							public boolean onMenuItemClick(MenuItem p1) {
								hideBookmarkAnimated();

								switch (p1.getItemId()) {
									case 0:
										Intent i = new Intent(MainActivity.this, EntryEditionActivity.class);
										startActivity(i);
										break;
									case 1:
										openSettings();
										break;
									case 2:
										Intent i2 = new Intent(MainActivity.this, EntriesListActivity.class);
										startActivity(i2);
										break;
								}
								return true;
							}
						});
					pm.show();
				}
			});
    }

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

		/* Commented due to nothing to hide
		 // Rezetting views that might be hidden
		 menu.setVisibility(View.VISIBLE);

		 // Hiding selected views
		 Set<String> hidden = prefs.getStringSet("UI_HIDE", new HashSet<String>());
		 for (String s : hidden) {
		 switch (s) {
		 case "BUTTONBAR":
		 menu.setVisibility(View.GONE);
		 break;
		 }
		 }	*/

		JournalModel.useTESDate = prefs.getBoolean("UI_CANONICAL_DATE", true);

		checkDb();
		loadMainList();

		hideBookmark();

		checkSync();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (db != null)
			db.close();
		db = null;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {

		switch (keyCode) {
			case KeyEvent.KEYCODE_MENU:
				if (bookmarksRoot.getVisibility() == View.VISIBLE) {
					hideBookmarkAnimated();
				} else {
					showBookmarkAnimated();
				}
				break;
			default:
				return super.onKeyUp(keyCode, event);
		}

		return true;
	}

	@Override
	public void onBackPressed() {
		if (bookmarksRoot.getVisibility() == View.VISIBLE) {
			hideBookmarkAnimated();
		} else {
			ExitActivity.exit(this);
		}
	}

	private void openSettings() {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
	}

	private void checkDb() {
		if (db == null)
			db = new Database(MainActivity.this);
	}

	private void updateNavBar() {
		if (model == null) return;

		if (model.showPrev()) {
			prev.setVisibility(View.VISIBLE);
		} else {
			prev.setVisibility(View.INVISIBLE);
		}

		if (model.showNext()) {
			next.setVisibility(View.VISIBLE);
		} else {
			next.setVisibility(View.INVISIBLE);
		}

		page1num.setText("" + model.getCurrentPageNum());
		if (page2num != null) page2num.setText("" + (model.getCurrentPageNum() + 1));
	}

	private void setLayoutListner(final TextView textView) {
		textView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					textView.getViewTreeObserver().removeGlobalOnLayoutListener(this);

					ArrayList<TextView> list = new ArrayList<TextView>();
					list.add(page1);
					if (page2 != null) list.add(page2);

					model = new JournalModel(list.toArray(new TextView[list.size()]));
					model.setUpdList(new JournalModel.UpdateListener(){

							@Override
							public void onUpdate(String url, String q) {
								if (q != null) {
									loadTitlesQuery(q);
								} else {
									loadMainList();
								}
							}
						});
					loadMainList();
				}
			});
	}

	private void loadMainList() {
		hideBookmarkAnimated();

		if (model != null) {
			model.set(db.getDiaryEntries(), true);
			updateNavBar();
		}
		close.setText(textClose);
	}

	private void loadTitlesQuery(String q) {
		hideBookmarkAnimated();

		if (model != null) {
			model.set(db.getEntriesByTitle(q), false);
			model.setCurrentPageNum(1);
			updateNavBar();
			close.setText(textJournal);
		}
	}

	private void loadChainsQuery(String q) {
		hideBookmarkAnimated();

		if (model != null) {
			int id = db.getChainId(q);
			if (id >= 0) {
				model.set(db.getEntriesByChain(id), true);
				model.setCurrentPageNum(model.getPagesCount());
				updateNavBar();
				close.setText(textJournal);
			}
		}
	}

	private void showBookmarkAnimated() {

		options.setVisibility(View.INVISIBLE);
		prev.setVisibility(View.INVISIBLE);
		next.setVisibility(View.INVISIBLE);
		close.setVisibility(View.INVISIBLE);

		page1.setClickable(false);
		if (page2 != null)
			page2.setClickable(false);

		loadBookmarkAlphabetical();

		bookmarksRoot.setTranslationY(-bookmarksRoot.getHeight());
		bookmarksRoot.setVisibility(View.VISIBLE);
		bookmarksRoot
			.animate()
			.translationY(0)
			.setListener(new AnimatorListenerAdapter() {

				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					// Just do nothing
				}
			});
	}

	private void loadBookmarkAlphabetical() {
		checkDb();

		getFragmentManager()
			.beginTransaction()
			.replace(R.id.bookmarks_overlay_frame, 
					 new BookmarksAlphabeticalFragment(db.getAllEntries(), alphQuery))
			.commit();
	}

	private BookmarksListener alphQuery = new BookmarksListener(){

		@Override
		public void onSelection(String data) {
			loadTitlesQuery(data);
		}
	};

	private void loadBookmarkQuests() {
		checkDb();

		getFragmentManager()
			.beginTransaction()
			.replace(R.id.bookmarks_overlay_frame, 
					 new BookmarksChainFragment(db.getAllUnendedChains(), questQuery))
			.commit();
	}

	private BookmarksListener questQuery = new BookmarksListener(){

		@Override
		public void onSelection(String data) {
			loadChainsQuery(data);
		}
	};

	private void hideBookmarkAnimated() {
		bookmarksRoot
			.animate()
			.translationY(-bookmarksRoot.getHeight())
			.setListener(new AnimatorListenerAdapter() {

				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					hideBookmark();
				}
			});
	}

	private void hideBookmark() {
		// Set visiblity to INVISIBLE because animation will not play on the first
		// try with GONE visiblity
		bookmarksRoot.setVisibility(View.INVISIBLE);
		options.setVisibility(View.VISIBLE);
		close.setVisibility(View.VISIBLE);

		updateNavBar();

		page1.setClickable(true);
		if (page2 != null)
			page2.setClickable(true);
	}

	private void checkSync() {

		if (ScheduledSyncAPI.available(this)) {
			final ArrayList<JSONObject> objects = ScheduledSyncAPI.examineAll(this);

			ArrayList<String> data = new ArrayList<String>();

			for (JSONObject object : objects) {
				data.add(new JournalEntry(object).toString());
			}

			String[] items = data.toArray(new String[data.size()]);

			if (items.length > 0) {
				final boolean[] checked = new boolean[items.length];

				AlertDialog.Builder ab = new AlertDialog.Builder(this);
				ab.setTitle("What do you want to import?");
				ab.setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener(){

						@Override
						public void onClick(DialogInterface p1, int p2, boolean p3) {
							checked[p2] = p3;
						}
					});
				ab.setPositiveButton("Import", new DialogInterface.OnClickListener(){

						@Override
						public void onClick(DialogInterface p1, int p2) {
							Database db = new Database(MainActivity.this);
							
							for (int i = 0; i < checked.length; i++) {
								if(checked[i]){
									JournalEntry e = new JournalEntry(objects.get(i));
									db.add(e);
								}
							}
							
							db.close();
							
							loadMainList();
							
							ScheduledSyncAPI.clearInboundList(MainActivity.this);
						}
					});
				ab.setNegativeButton("Later", null);
				ab.show();
			}
		}
	}
}
