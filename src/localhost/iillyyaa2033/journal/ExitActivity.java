package localhost.iillyyaa2033.journal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ExitActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 20) {
			finishAndRemoveTask();
		} else {
			finish();
		}
		
		System.exit(0);
	}

	public static void exit(Context c) {
		if (c == null) return;
		
		Intent i = new Intent(c, ExitActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				   Intent.FLAG_ACTIVITY_CLEAR_TASK |
				   Intent.FLAG_ACTIVITY_NO_ANIMATION |
				   Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		c.startActivity(i);
	}
}
