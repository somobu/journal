package localhost.iillyyaa2033.journal.bookmark;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import localhost.iillyyaa2033.journal.R;
import localhost.iillyyaa2033.journal.model.BookmarksListener;

public class BookmarksChainFragment extends Fragment {

	private static String[] data = new String[]{};
	private static BookmarksListener onQuery;
	
	public BookmarksChainFragment() {
	}

	public BookmarksChainFragment(String[] data, BookmarksListener query) {
		this.data = data;
		this.onQuery = query;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ListView list = new ListView(getActivity());
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, R.id.text1, data);
		list.setAdapter(adapter);
		list.setDividerHeight(0);
		list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					if(onQuery != null){
						onQuery.onSelection(adapter.getItem(p3));
					}
				}
			});
		return list;
	}


}
