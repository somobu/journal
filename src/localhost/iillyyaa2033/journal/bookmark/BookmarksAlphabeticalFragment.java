package localhost.iillyyaa2033.journal.bookmark;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import localhost.iillyyaa2033.journal.model.BookmarksListener;
import localhost.iillyyaa2033.journal.model.JournalEntry;

public class BookmarksAlphabeticalFragment extends Fragment {
	
	private static JournalEntry[] data;
	private static BookmarksAlphabeticalAdapter adapter;
	private static BookmarksListener onQuery;
	
	public BookmarksAlphabeticalFragment(){
		
	}
	
	public BookmarksAlphabeticalFragment(JournalEntry[] data, BookmarksListener query){
		this.data = data;
		this.onQuery = query;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if(data == null) data = new JournalEntry[0];
		
		ListView list = new ListView(getActivity());
		adapter = new BookmarksAlphabeticalAdapter(getActivity());
		adapter.set(data);
		list.setAdapter(adapter);
		list.setDividerHeight(0);
		list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					if(onQuery != null && !adapter.isSection(p3)){
						onQuery.onSelection(adapter.getItemClear(p3));
					}
				}
			});
		return list;
	}
}
