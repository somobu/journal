package localhost.iillyyaa2033.journal.model;

import java.util.Date;
import org.json.JSONObject;
import org.json.JSONException;

public class JournalEntry implements Comparable {

	public static final String DIARY_PREFIX = "¡";

	public int id;
	public String header;
	public String content;
	public int chain;

	public JournalEntry(String header, String content) {
		this(-1, header, content, -1);
	}

	public JournalEntry(int id, String header, String content, int chain) {
		this.id = id;
		this.header = header;
		this.content = content;
		this.chain = chain;
	}
	
	public JournalEntry(JSONObject item){
		this.id = -1;
/*		try {
			this.id = item.getInt("id");
		} catch (JSONException e) {}	*/
		try {
			this.header = item.getString("header");
		} catch (JSONException e) {}
		try {
			this.content = item.getString("content");
		} catch (JSONException e) {}
		try {
			this.chain = item.getInt("chain");
		} catch (JSONException e) {}
	}

	@Override
	public String toString() {
		return "‹‹" + getFancyHeader() + "››\n" + content;
	}

	public String toString(boolean withHeader) {
		if (withHeader) return "‹‹" + getFancyHeader() + "››\n" + content;
		else return content;
	}

	@Override
	public int compareTo(Object p1) {
		if (p1 instanceof JournalEntry) {
			return this.header.toLowerCase()
				.compareTo(((JournalEntry) p1).header.toLowerCase());
		} else {
			return -1;
		}
	}

	public boolean isDiaryEntry() {
		if (header == null) return false;
		return header.startsWith(DIARY_PREFIX);
	}

	public String getFancyHeader() {
		if (isDiaryEntry()) {
			String s = "";
			try {
				long l = Long.parseLong(header.substring(DIARY_PREFIX.length()));
				Date d = new Date(l);
				
				if (JournalModel.useTESDate) {
					s = TesDateFormatter.format(d);
				} else {
					s = d.toLocaleString();
				}
			} catch (Exception e) {}
			return s;
		} else {
			return header;
		}
	}
	
	public JSONObject toJson(){
		JSONObject object = new JSONObject();
		
		try {
			object.put("id", id);
		} catch (JSONException e) {}
		try {
			object.put("header", header);
		} catch (JSONException e) {}
		try {
			object.put("content", content);
		} catch (JSONException e) {}
		try {
			object.put("chain", chain);
		} catch (JSONException e) {}

		return object;
	}
}
