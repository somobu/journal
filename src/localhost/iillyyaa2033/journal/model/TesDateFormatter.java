package localhost.iillyyaa2033.journal.model;

import java.util.Date;

public class TesDateFormatter {

	public static String[] months = new String[12];

	public static String format(Date date) {
		return String.format("%d %s", date.getDate(), months[date.getMonth()]);
	}
}
