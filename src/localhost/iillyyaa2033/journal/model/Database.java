package localhost.iillyyaa2033.journal.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import localhost.iillyyaa2033.sync.api.ScheduledSyncAPI;

public class Database {

	private SQLiteDatabase db;

	public Database(Context context) {
		MyDBOpenHelper helper = new MyDBOpenHelper(context, null);
		db = helper.getWritableDatabase();
	}

	public void close() {
		db.close();
	}


	private static final String TABLE_RECORDS = "records";
	private static final String COLUMN_RECORDS_ID = "_id";
	private static final String COLUMN_RECORDS_TITLE = "title";
	private static final String COLUMN_RECORDS_DATE = "date";
	private static final String COLUMN_RECORDS_MESSAGE = "message";
	private static final String COLUMN_RECORDS_CHAIN = "chain";
	private static final String COLUMN_RECORDS_HASH = "hash";

	private static final String CREATE_TABLE_RECORDS = 
	"create table " + TABLE_RECORDS + "(" +
	COLUMN_RECORDS_ID + " integer primary key autoincrement, " +
	COLUMN_RECORDS_TITLE + " text not null, " +
	COLUMN_RECORDS_DATE + " text not null, " +
	COLUMN_RECORDS_MESSAGE + " text not null," +
	COLUMN_RECORDS_CHAIN + " integer," +
	COLUMN_RECORDS_HASH + " text not null" +
	");";

	private static final String[] RECORDS = new String[]{
		COLUMN_RECORDS_ID,
		COLUMN_RECORDS_TITLE,
		COLUMN_RECORDS_MESSAGE,
		COLUMN_RECORDS_CHAIN,
		COLUMN_RECORDS_HASH
	};

	public int add(JournalEntry e) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_RECORDS_DATE, "" + System.currentTimeMillis());
		values.put(COLUMN_RECORDS_TITLE, e.header);
		values.put(COLUMN_RECORDS_MESSAGE, e.content);
		values.put(COLUMN_RECORDS_HASH, ScheduledSyncAPI.calculateHash(e.toJson()));

		if (e.chain >= 0) {
			values.put(COLUMN_RECORDS_CHAIN, e.chain);
		}

		if (e.id >= 0) {
			db.update(TABLE_RECORDS, values, COLUMN_RECORDS_ID + " = " + e.id, null);
			return e.id;
		} else {
			return (int) db.insert(TABLE_RECORDS, null, values);
		}
	}

	public JournalEntry[] getAllEntries() {

		ArrayList<JournalEntry> result = new ArrayList<JournalEntry>();

		Cursor c = db.query(TABLE_RECORDS, RECORDS, null, null, null, null, 
							COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			do {
				result.add(new JournalEntry(c.getInt(0), c.getString(1), 
											c.getString(2), c.getInt(3)));
			} while(c.moveToNext());
		}

		return result.toArray(new JournalEntry[result.size()]);
	}

	public JournalEntry[] getDiaryEntries() {

		ArrayList<JournalEntry> result = new ArrayList<JournalEntry>();

		Cursor c = db.query(TABLE_RECORDS, RECORDS, null, null, null, null, 
							COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			do {
				String header = c.getString(1);
				if (header.startsWith(JournalEntry.DIARY_PREFIX))
					result.add(new JournalEntry(c.getInt(0), header, 
												c.getString(2), c.getInt(3)));
			} while(c.moveToNext());
		}

		return result.toArray(new JournalEntry[result.size()]);
	}

	public JournalEntry[] getEntriesByTitle(String title) {

		ArrayList<JournalEntry> result = new ArrayList<JournalEntry>();

		Cursor c = db.query(TABLE_RECORDS, RECORDS, null, null, null, null, 
							COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			do {
				String header = c.getString(1);
				if (header.toLowerCase().contains(title.toLowerCase()))
					result.add(new JournalEntry(c.getInt(0), header,
												c.getString(2), c.getInt(3)));
			} while(c.moveToNext());
		}
		return result.toArray(new JournalEntry[result.size()]);
	}

	public JournalEntry[] getEntriesByChain(int chainId) {

		ArrayList<JournalEntry> result = new ArrayList<JournalEntry>();

		Cursor c = db.query(TABLE_RECORDS, RECORDS,
							COLUMN_RECORDS_CHAIN + " = " + chainId, null, null, null, 
							COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			do {
				result.add(new JournalEntry(c.getInt(0), c.getString(1),
											c.getString(2), c.getInt(3)));
			} while(c.moveToNext());
		}

		return result.toArray(new JournalEntry[result.size()]);
	}

	public JournalEntry getEntryById(String id) {

		Cursor c = db.query(TABLE_RECORDS, RECORDS, COLUMN_RECORDS_ID + " = " + id, 
							null, null, null, COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			return new JournalEntry(c.getInt(0), c.getString(1), 
									c.getString(2), c.getInt(3));
		}

		return null;
	}

	public JournalEntry getEntryByHash(String hash) {

		Cursor c = db.query(TABLE_RECORDS, RECORDS, COLUMN_RECORDS_HASH + " = " + hash, 
							null, null, null, COLUMN_RECORDS_DATE);

		if (c.moveToFirst()) {
			return new JournalEntry(c.getInt(0), c.getString(1), 
									c.getString(2), c.getInt(3));
		}

		return null;
	}

	public Database removeEntry(int id) {
		db.delete(TABLE_RECORDS, COLUMN_RECORDS_ID + " = " + id, null);
		return this;
	}


	private static final String TABLE_CHAINS = "chains";
	private static final String COLUMN_CHAINS_ID = "_id";
	private static final String COLUMN_CHAINS_NAME = "name";
	private static final String COLUMN_CHAINS_ENDED = "ended";

	private static final String CREATE_TABLE_CHAINS = 
	"create table " + TABLE_CHAINS + "(" +
	COLUMN_CHAINS_ID + " integer primary key autoincrement, " +
	COLUMN_CHAINS_NAME + " text not null," +
	COLUMN_CHAINS_ENDED + " text not null" +
	");";

	String[] CHAINS = new String[]{
		COLUMN_CHAINS_ID,
		COLUMN_CHAINS_NAME,
		COLUMN_CHAINS_ENDED
	};

	public String[] getAllChains() {
		ArrayList<String> result = new ArrayList<String>();

		Cursor c = db.query(TABLE_CHAINS, new String[]{COLUMN_CHAINS_NAME}, 
							null, null, null, null, COLUMN_CHAINS_NAME);

		if (c.moveToFirst()) {
			do {
				result.add(c.getString(0));
			} while(c.moveToNext());
		}

		return result.toArray(new String[result.size()]);
	}

	public String[] getAllUnendedChains() {
		ArrayList<String> result = new ArrayList<String>();

		Cursor c = db.query(TABLE_CHAINS, new String[]{COLUMN_CHAINS_NAME,COLUMN_CHAINS_ENDED}, 
							null, null, null, null, COLUMN_CHAINS_NAME);

		if (c.moveToFirst()) {
			do {
				if (c.getString(1).equals("false"))
					result.add(c.getString(0));
			} while(c.moveToNext());
		}

		return result.toArray(new String[result.size()]);
	}

	public Cursor getChainsCursor() {
		return db.query(TABLE_CHAINS, new String[]{COLUMN_CHAINS_ID,
							COLUMN_CHAINS_NAME, COLUMN_CHAINS_ENDED}, 
						null, null, null, null, COLUMN_CHAINS_ID);
	}

	public int getChainId(String chain) {

		Cursor c = db.query(TABLE_CHAINS, CHAINS, 
							COLUMN_CHAINS_NAME + " = \"" + chain + "\"",
							null, null, null, COLUMN_CHAINS_NAME);

		if (c.moveToFirst()) {
			return c.getInt(0);
		}

		return -1;
	}

	public String getChain(int id) {
		Cursor c = db.query(TABLE_CHAINS, CHAINS, COLUMN_CHAINS_ID + " = " + id,
							null, null, null, COLUMN_CHAINS_NAME);

		if (c.moveToFirst()) {
			return c.getString(1);
		}

		return null;
	}

	public int addChain(String chain) {
		ContentValues cv = new ContentValues();

		cv.put(COLUMN_CHAINS_NAME, chain);
		cv.put(COLUMN_CHAINS_ENDED, "false");

		db.insert(TABLE_CHAINS, null, cv);
		return getChainId(chain);
	}

	public int addChain(String chain, boolean ended) {
		ContentValues cv = new ContentValues();

//		cv.put(COLUMN_CHAINS_ID, id);
		cv.put(COLUMN_CHAINS_NAME, chain);
		cv.put(COLUMN_CHAINS_ENDED, ended);

		db.insert(TABLE_CHAINS, null, cv);
		return getChainId(chain);
	}

	public Database updateChainState(String chain, boolean state) {
		int id = getChainId(chain);

		if (id < 0) {
			addChain(chain, state);
		} else {
			ContentValues cv = new ContentValues();

			cv.put(COLUMN_CHAINS_NAME, chain);
			cv.put(COLUMN_CHAINS_ENDED, state ? "true" : "false");

			db.update(TABLE_CHAINS, cv, COLUMN_CHAINS_ID + " = " + id, null);
		}

		return this;
	}

	private static class MyDBOpenHelper extends SQLiteOpenHelper {

		static final String name = "journal";
		static final int version = 2;

		public MyDBOpenHelper(Context context, SQLiteDatabase.CursorFactory factory) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_RECORDS);
			db.execSQL(CREATE_TABLE_CHAINS);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int from, int to) {
			switch (from) {
				case 1:	// DO UPGRADE FROM 1 TO 2
					db.execSQL("ALTER TABLE "+TABLE_RECORDS+" ADD "+COLUMN_RECORDS_HASH+" text");
				case 2:	// DO UPGRADE FROM 2 TO 3
					// ...
			}
		}
	}
}
