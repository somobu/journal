package localhost.iillyyaa2033.journal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import localhost.iillyyaa2033.journal.EntryEditionActivity;
import localhost.iillyyaa2033.journal.R;
import localhost.iillyyaa2033.journal.model.Database;
import localhost.iillyyaa2033.journal.model.JournalEntry;
import localhost.iillyyaa2033.sync.api.ScheduledSyncAPI;

public class EntryEditionActivity extends Activity {

	private boolean useMySync = false;	// True if we need to sync via my program

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);

		if (metrics.widthPixels < 500 || metrics.heightPixels < 500) {
			setTheme(R.style.AppTheme);
		}

		getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.dialog_newentry);

		useMySync = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("SYNC_BY_IILLYYAA2033", false);

		Intent intent = getIntent();
		final int id = intent.getIntExtra("journalentry_id", -1);


		Database db = new Database(EntryEditionActivity.this);
		String[] chains = db.getAllChains();
		db.close();

		final AutoCompleteTextView header = (AutoCompleteTextView) findViewById(R.id.dialog_newentry_header);
		final AutoCompleteTextView chain = (AutoCompleteTextView) findViewById(R.id.dialog_entry_chain);

		ArrayAdapter<String> chainAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, chains);
		chain.setAdapter(chainAdapter);
		chain.setThreshold(1);

		final EditText message = (EditText) findViewById(R.id.dialog_newentry_message);
		final CheckBox isFinal = (CheckBox) findViewById(R.id.dialog_newentry_isfinal);

		final Spinner sp = (Spinner) findViewById(R.id.dialog_newentry_spinner);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
																new String[]{getString(R.string.event),getString(R.string.topic)});
		sp.setAdapter(adapter);
		sp.setOnItemSelectedListener(new OnItemSelectedListener(){

				@Override
				public void onItemSelected(AdapterView<?> p1, View p2, int p3, long p4) {
					switch (p3) {
						case 0:
							header.setVisibility(View.GONE);
							chain.setVisibility(View.VISIBLE);
							isFinal.setVisibility(View.VISIBLE);
							break;
						default:
							header.setVisibility(View.VISIBLE);
							chain.setVisibility(View.GONE);
							isFinal.setVisibility(View.GONE);
							break;
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> p1) {
					// TODO: Implement this method
				}
			});
		sp.setSelection(0);

		((Button) findViewById(R.id.dialog_entry_ok)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					Database db = new Database(EntryEditionActivity.this);

					String headerString;
					int chainId;

					if (sp.getSelectedItemId() == 0) {	// Event (quest)
						if (id >= 0) {
							headerString = header.getText().toString();
						} else {
							headerString = JournalEntry.DIARY_PREFIX + System.currentTimeMillis();
						}

						String chainName = chain.getText().toString();
						if (chainName.isEmpty()) {
							chainId = -1;
						} else {
							chainId = db.getChainId(chainName);
							if (chainId < 0) {
								chainId = db.addChain(chainName);
							}

							db.updateChainState(chainName, isFinal.isChecked());
						}
					} else {	// Topic
						headerString = header.getText().toString();
						chainId = -1;
					}

					String messageString = message.getText().toString();


					JournalEntry oldEntry = null;
					JournalEntry newEntry = new JournalEntry(id, headerString, messageString, chainId);

					if (id >= 0) {
						oldEntry =  db.getEntryById("" + id);
					}

					if (useMySync && ScheduledSyncAPI.available(EntryEditionActivity.this)) {
						String hash = "";

						if (oldEntry != null)
							hash = ScheduledSyncAPI.calculateHash(oldEntry.toJson());

						ScheduledSyncAPI.insert(EntryEditionActivity.this, hash, newEntry.toJson());
					}

					db.add(newEntry);
					db.close();
					finish();
				}
			});

		((Button) findViewById(R.id.dialog_entry_cancel)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					finish();
				}
			});

		String action = intent.getAction();
		if (Intent.ACTION_SEND.equals(action)) {

			if (intent.hasExtra(Intent.EXTRA_TITLE)) {
				header.setText(intent.getStringExtra(Intent.EXTRA_TITLE));
				chain.setText(intent.getStringExtra(Intent.EXTRA_TITLE));
			}

			if (intent.hasExtra(Intent.EXTRA_TEXT)) {
				message.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
			}
		}

		if (id >= 0) {
			Database jdb = new Database(this);
			JournalEntry e = jdb.getEntryById("" + id);

			if (e.isDiaryEntry()) {
				sp.setSelection(0);
				if (e.chain >= 0) {
					chain.setText(jdb.getChain(e.chain));
				}
			} else {
				sp.setSelection(1);
			}

			sp.setEnabled(false);
			header.setText(e.header);
			message.setText(e.content);

			jdb.close();
		}
	}
}
