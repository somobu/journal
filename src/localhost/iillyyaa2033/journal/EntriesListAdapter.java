package localhost.iillyyaa2033.journal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import localhost.iillyyaa2033.journal.model.Database;
import localhost.iillyyaa2033.journal.model.JournalEntry;

public class EntriesListAdapter extends BaseAdapter {

	private Context context;
	private JournalEntry[] entries;
	private OnButtonClick listener;
	
	public EntriesListAdapter(Context c) {
		context = c;
		reloadList();
	}

	@Override
	public int getCount() {
		return entries.length;
	}

	@Override
	public JournalEntry getItem(int p1) {
		return entries[p1];
	}

	@Override
	public long getItemId(int p1) {
		return p1;
	}

	@Override
	public View getView(int p1, View p2, ViewGroup p3) {
		if (p2 == null) {
			p2 = LayoutInflater.from(context).inflate(R.layout.dialog_entrieslist_item, null, false);
		}

		final JournalEntry entry = this.getItem(p1);

		((TextView) p2.findViewById(R.id.dialog_entrieslist_item_title)).setText(entry.getFancyHeader());
		((TextView) p2.findViewById(R.id.dialog_entrieslist_item_text)).setText(entry.toString(false));

		((ImageButton) p2.findViewById(R.id.dialog_entrieslist_item_edit)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					if(listener != null)
						listener.edit(entry);
				}
		});
		
		((ImageButton) p2.findViewById(R.id.dialog_entrieslist_item_remove)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					if(listener != null)
						listener.remove(entry);
				}
			});
		
		return p2;
	}

	public void reloadList(){
		Database db = new Database(context);
		entries = db.getAllEntries();
		db.close();
	}
	
	public void setListener(OnButtonClick l){
		this.listener = l;
	}
	
	public interface OnButtonClick {
		public void edit(JournalEntry e);
		public void remove(JournalEntry e);
	}
}
