package localhost.iillyyaa2033.journal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import localhost.iillyyaa2033.journal.model.Database;
import localhost.iillyyaa2033.journal.model.JournalEntry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.preference.CheckBoxPreference;
import localhost.iillyyaa2033.sync.api.ScheduledSyncAPI;

public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);

		/*	Preference uiHide = findPreference("UI_HIDE");
		 uiHide.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){

		 @Override
		 public boolean onPreferenceClick(Preference p1) {
		 showHideUiDialog();
		 return true;
		 }
		 });	*/

		Preference ioExport = findPreference("IO_EXPORT");
		ioExport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){

				@Override
				public boolean onPreferenceClick(Preference p1) {
					showExportDialog();
					return true;
				}
			});

		Preference ioImport = findPreference("IO_IMPORT");
		ioImport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){

				@Override
				public boolean onPreferenceClick(Preference p1) {
					showImportDialog();
					return true;
				}
			});
			
			
		CheckBoxPreference syncI3 = (CheckBoxPreference) findPreference("SYNC_BY_IILLYYAA2033");
		syncI3.setEnabled(ScheduledSyncAPI.available(this));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			try {
				InputStream is = getContentResolver().openInputStream(data.getData());
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);

				String jsonStr = "";
				String line = null;
				try {
					while ((line = br.readLine()) != null) {
						jsonStr += line + "\n";
					}
				} catch (IOException e) {e(e);}

				try {
					JSONObject json = new JSONObject(jsonStr);

					Database db = new Database(this);
					JSONArray chains = json.getJSONArray("chains");
					for (int i = 0; i < chains.length(); i++) {
						JSONObject obj = chains.getJSONObject(i);
						db.addChain(obj.getString("name"), obj.getString("ended").equals("true"));
					}

					JSONArray records = json.getJSONArray("records");
					for (int i = 0; i < records.length(); i++) {
						JournalEntry e = new JournalEntry(records.getJSONObject(i));
						int q = db.add(e);
					}
				} catch (JSONException e) {e(e);}

				try { br.close(); } catch (IOException e) {e(e);}
				try { isr.close(); } catch (IOException e) {e(e);}
				try { is.close(); } catch (IOException e) {e(e);}
			} catch (FileNotFoundException e) {e(e);}
		}
	}

	void showHideUiDialog() {
		String[] items = getResources().getStringArray(R.array.pref_hideui_list);
		final boolean[] selected = {false};

		Set<String> set = PreferenceManager.getDefaultSharedPreferences(this).getStringSet("UI_HIDE", new HashSet<String>());
		for (String s : set) {
			switch (s) {
				case "BUTTONBAR":
					selected[0] = true;
					break;
			}
		}

		new AlertDialog.Builder(this)
			.setTitle(R.string.pref_hideui_dialog)
			.setMultiChoiceItems(items, selected, new DialogInterface.OnMultiChoiceClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2, boolean p3) {
					selected[p2] = p3;
				}
			})
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2) {
					HashSet<String> set = new HashSet<String>();
					if (selected[0]) set.add("BUTTONBAR");

					PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this).edit().putStringSet("UI_HIDE", set).commit();
				}
			})
			.show();
	}

	void showExportDialog() {
		Database db = new Database(this);
		JSONObject jsonRoot = new JSONObject();

		Cursor chains = db.getChainsCursor();
		if (chains.moveToFirst()) {
			JSONArray jsonChains = new JSONArray();
			do {
				JSONObject jsonChain = new JSONObject();
				try {
					jsonChain.put("id", chains.getString(0));
					jsonChain.put("name", chains.getString(1));
					jsonChain.put("ended", chains.getString(2));
					jsonChains.put(jsonChain);
				} catch (JSONException e) {e(e);}
			} while(chains.moveToNext());

			try {
				jsonRoot.put("chains", jsonChains);
			} catch (JSONException e) {e(e);}
		}

		JournalEntry[] entries = db.getAllEntries();
		db.close();

		JSONArray jsonEntries = new JSONArray();
		for (JournalEntry entry : entries) {
			jsonEntries.put(entry.toJson());
		}

		try {
			jsonRoot.put("records", jsonEntries);
		} catch (JSONException e) {e(e);}


		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, jsonRoot.toString());
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	void showImportDialog() {
		Intent intentTxt = new Intent(Intent.ACTION_GET_CONTENT);
		intentTxt.setType("*/*");
		intentTxt.addCategory(Intent.CATEGORY_OPENABLE);
		startActivityForResult(intentTxt, 5);
	}

	void e(Exception e) {
		Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
	}
}
